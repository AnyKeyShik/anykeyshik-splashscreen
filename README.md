# Plasma Splashscreen White New Tux

This Plasma Splashscreen is a fork of [ManjaroLogoBlack](https://gitlab.com/menelkir/manjarologoblack) by [Daniel Menelkier](https://gitlab.com/menelkir) and reworked by [AnyKeyShik Rarity](https://gitlab.com/AnyKeyShik).

A splash theme for KDE 6 based on archlogogreen

![](./contents/previews/splash.png)


## Based on

+ [ManjaroLogoBlack](https://gitlab.com/menelkir/manjarologoblack) by [Daniel Menelkier](https://gitlab.com/menelkir)
+ [archlogogreen](https://www.opendesktop.org/p/997749/) by [sequizz](https://www.pling.com/u/seqizz/)
+ [New Tux](https://store.kde.org/p/1082435) by lewing@isc.tamu.edu Larry Ewing and The GIMP
+ [Simple Arch blue](https://github.com/kraken-afk/archsimpleblue-kde6)

